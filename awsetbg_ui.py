#!/usr/bin/python3.2

#
#	AUTHOR:			DoctorMe
#	DATE:			11/12/11
#	DESCRIPTION:	Something to make changing wallpapers in Awesome WM a little easier
#

import os
import sys

from tkinter import *			# import all the standard tk stuff
from tkinter import filedialog	# import tk's filedialog dialog
from tkinter import ttk			# import all the ttk stuff as "ttk"

#
#	The main class for the application
#
class feh_ui:
	#
	#	constructor
	#
	def __init__ (self):
		# set up the main window
		self.rootWin = Tk ()
		self.rootWin.title ("awsetbg UI")
		self.rootWin.geometry ("300x200")
		
		self.mainFrame = ttk.Frame (self.rootWin)
		
		# file picker
		self.filePicker_frame = ttk.Frame (self.mainFrame)
		self.wallFile = StringVar ()
		ttk.Label (self.filePicker_frame, text = "Pick your wallpaper").pack (side = TOP)
		self.filePath_field = ttk.Entry (self.filePicker_frame, textvariable = self.wallFile)
		self.filePath_field.pack (side = LEFT, expand = YES, fill = X)
		self.filePickerButton_button = ttk.Button (self.filePicker_frame, text = "Find File", command = self.find)
		self.filePickerButton_button.pack (side = RIGHT, expand = NO, fill = NONE)
		self.filePicker_frame.pack (side = TOP, expand = YES, fill = X)
		
		# wallpaper mode radio buttons
		self.modeRadios_frame = ttk.Frame (self.mainFrame)
		# what mode to display the wallpaper at?
		# tk requires variables that're hooked up to widgets be instances of its special var classes
		# so that it knows how to handle them. Also, they must be defined AFTER "self.root = Tk()"
		self.wallMode = StringVar ()
		ttk.Label (self.modeRadios_frame, text = "Pick the display mode").grid (row = 0, column = 0)
		self.scale_radio = ttk.Radiobutton (self.modeRadios_frame, text = "Fullscreen", variable = self.wallMode, value = "-f")
		self.scale_radio.grid (row = 1, column = 0)
		self.tile_radio = ttk.Radiobutton (self.modeRadios_frame, text = "Tile", variable = self.wallMode, value = "-t")
		self.tile_radio.grid (row = 1, column = 1)
		self.center_radio = ttk.Radiobutton (self.modeRadios_frame, text = "Center", variable = self.wallMode, value = "-c")
		self.center_radio.grid (row = 1, column = 2)
		self.max_radio = ttk.Radiobutton (self.modeRadios_frame, text = "Maximize", variable = self.wallMode, value = "-a")
		self.max_radio.grid (row = 1, column = 3)
		self.modeRadios_frame.pack (side = TOP, expand = YES, fill = X)
		
		# ok, apply, cancel
		self.okApplyCancel_frame = ttk.Frame (self.mainFrame)
		self.cancel_button = ttk.Button (self.okApplyCancel_frame, text = "Cancel", command = self.close)
		self.cancel_button.pack (side = RIGHT)
		self.apply_button = ttk.Button (self.okApplyCancel_frame, text = "Apply", command = self.applie)
		self.apply_button.pack (side = RIGHT)
		self.ok_button = ttk.Button (self.okApplyCancel_frame, text = "OK", command = self.OK)
		self.ok_button.pack (side = RIGHT)
		self.okApplyCancel_frame.pack (side = BOTTOM)
		
		
		self.mainFrame.pack (side = TOP, expand = YES, fill = BOTH)
		
		self.rootWin.mainloop ()
		return
	
	#
	#	close the program
	#
	def close (self):
		print ("terminating...")
		self.rootWin.quit ()
		return
	
	#
	#	apply all the settings but don't close
	#	(the spelling is wierd to dodge the keyword)
	#
	def applie (self):
		print ("Apply...")
		self.command_str = 'awsetbg %s %s' % (self.wallMode.get (), self.wallFile.get ())
		print (self.command_str)
		os.system (self.command_str)
		return
	
	#
	#	apply all the settings and close the program
	#
	def OK (self):
		self.applie ()
		self.close ()
		return
	
	#
	#	pop up the file browser dialog and pick the file
	#
	def find (self):
		print ("find...")
		# pop up the file finder dialog
		self.wallFile.set (filedialog.askopenfilename (parent = self.rootWin, title = "Choose your wallpaper"))
		return


#
#	ok, execute!
#
if (__name__ == "__main__"):
	app = feh_ui()



