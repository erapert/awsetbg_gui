awsetbg_gui
===========

A quick little GUI for [awsetbg](http://awesome.naquadah.org/wiki/Awsetbg_3.1) for [AwesomeWM](http://awesome.naquadah.org/)

###History
I use AwesomeWM as my primary desktop environment on Arch and Ubuntu-- I don't do any fancy config to, say, hook Awesome up to Gnome to use it as a window manager for Gnome. No, I just use Awesome.

In order to set the background in Awesome you must use the command line tool `awsetbg`. But it quickly becomes tedious to look up the command switches every time you want to set your wallpaper. So I wrote a little GUI wrapper around the command line tool using Python3. I'm sure you could write a sweet Awesome plugin using Lua, but I'm familiar with Python and **not** familiar with Awesome's Lua API.

##Installation

1. ####Install the pre-reqs:
	* *Ubuntu (14.04) Python:* `sudo apt-get install python3 python3-tk git feh`
	* *Ubuntu (14.04) Ruby:* `sudo apt-get install ruby libtcltk-ruby git feh`
	* *Arch:* `sudo pacman -S python tk git feh`
	* *Fedora (18):* `sudo yum install python3 python3-tkinter git feh`

2. ####Get the GUI program:
	<pre>
	git clone https://github.com/doctorme/awsetbg_gui.git
	</pre>

3. ####Run it:
	<pre>python ./awsetbg_gui/awsetbg_ui.py</pre>
    or
    <pre>ruby ./awsetbg_gui/awsetbg_ui.rb</pre>
	<small>For Ubuntu you'll need to do: `python3 ./awsetbg_gui/awsetbg_ui.py`</small>


###FAQ

#####Conventions:
* `CTRL+C` means hold down the `control` key and press `c` -- just like copying a file in Windows.
* likewise, `META+R` means hold down the Windows key on your keyboard and press `r` -- in Linux, the Windows key is called the "meta" key.

**Q:** Everything works fine 'till I restart Awesome and then it goes right back to the old wallpaper. How do I make it use the last wallpaper that I set?

**A:** Awesome is all about the ability to customize and configure everything to your heart's content. The way to properly solve your problem isn't hard, but it's a little more involved than clicking a button. For the following instructions I'll assume you've got a default installation of Awesome (i.e. you've done absolutely no customization, you merely did `sudo apt-get install awesome`). Bear with me, here we go:

1. Open up a terminal: `CTRL+ENTER`
2. Get your own local copy of Awesome's rc.lua:
	`cp /etc/xdg/awesome/rc.lua ~/.config/awesome/rc.lua`
3. Open `~/.config/awesome/rc.lua` in Gedit.
4. Find the line that says something like:
	<pre>
	-- Themes define colours, icons, and wallpapers
	beautiful.init("/usr/share/awesome/themes/default/theme.lua")
	</pre>
	And change it to something like this:
	<pre>
	-- Themes define colours, icons, and wallpapers
	--beautiful.init("/usr/share/awesome/themes/default/theme.lua")
	beautiful.init("/home/YOUR_USERNAME/.config/awesome/mytheme.lua")
	</pre>
	Then save it.
5. Now let's make sure we didn't screw up the syntax of the config file. In your terminal do this: `awesome -k` and it should say `✔ Configuration file syntax OK.` If not then make sure you get the syntax right before proceeding.
6. Now let's make our personal theme file just like we did the `rc.lua`. In your terminal do this: `cp /usr/share/awesome/themes/default/theme.lua ~/.config/awesome/mytheme.lua`
7. Now open up `~/.config/awesome/mytheme.lua` in gedit.
8. Find the line that looks like this:
	<pre>
	-- You can use your own command to set your wallpaper
	theme.wallpaper_cmd = { "awsetbg /usr/share/awesome/themes/default/background.png" }
	</pre>
	And change it to this:
	<pre>
	-- You can use your own command to set your wallpaper
	--theme.wallpaper_cmd = { "awsetbg /usr/share/awesome/themes/default/background.png" }
	theme.wallpaper_cmd = { "awsetbg -l" }
	</pre>
9. Now, finally, use awsetbg_ui.py to set your wallpaper. Then close all programs and exit/restart Awesome. It should restore the last wallpaper you set with awsetbg_ui.py. If things still aren't working try doing `awsetbg -l` manually in a terminal to see if that works. If it does then make sure you entered the right paths in your `rc.lua` and `mytheme.lua` files.
