require 'tk'
require 'tkextlib/tile'

class UI
	public
		def initialize
			@mainwindow = TkRoot.new { title 'awsetbg_ui' }
			buildUI @mainwindow
		end
		
		def run
			Tk.mainloop
		end
		
		def onClose
			@mainwindow.destroy
		end
		
		def onApply
			`awsetbg #{$wallMode} #{$wallFile}`
		end
		
		def onOK
			onApply
			onClose
		end
		
		def find
			$wallFile.set_value(Tk::getOpenFile(){ parent @mainwindow; title 'Choose your wallpaper' })
		end
	
	private
		def buildUI root
			mainFrame = Tk::Tile::Frame.new(root).pack :expand => true, :fill => :both
			filePickerFrame = Tk::Tile::Frame.new(mainFrame).pack
				# tk vars have to be global?! wtf?!
				$wallFile = TkVariable.new
				Tk::Tile::Label.new(filePickerFrame){ text 'Pick your wallpaper' }.pack :side => :top
				filePathField = Tk::Tile::Entry.new(filePickerFrame){
					textvariable $wallFile
					}.pack :side => :left, :expand => true, :fill => :both
				filePickerButton = Tk::Tile::Button.new(filePickerFrame){
					text 'Find File'
					}.pack :side => :top, :expand => true, :fill => :x
					filePickerButton['command'] = lambda { find }
			
			modeRadiosFrame = Tk::Tile::Frame.new(mainFrame).pack :side => :top, :expand => true, :fill => :x
				$wallMode = TkVariable.new
				Tk::Tile::Label.new(modeRadiosFrame){
					text 'Pick the display mode'
					}.grid :row => 0, :column => 0
				Tk::Tile::RadioButton.new(modeRadiosFrame){
					text 'Fullscreen'; variable $wallMode; value '-f'
					}.grid :row => 1, :column => 0
				Tk::Tile::RadioButton.new(modeRadiosFrame){
					text 'Tile'; variable $wallMode; value '-t'
					}.grid :row => 1, :column => 1
				Tk::Tile::RadioButton.new(modeRadiosFrame){
					text 'Center'; variable $wallMode; value '-c'
					}.grid :row => 1, :column => 2
				Tk::Tile::RadioButton.new(modeRadiosFrame){
					text 'Maximize'; variable $wallMode; value '-a'
					}.grid :row => 1, :column => 3
			
			okFrame = Tk::Tile::Frame.new(mainFrame).pack :side => :bottom
				cancelBtn = Tk::Tile::Button.new(okFrame){ text 'Cancel' }.pack :side => :right
					cancelBtn['command'] = lambda { onClose }
				applyBtn = Tk::Tile::Button.new(okFrame){ text 'Apply' }.pack :side => :right
					applyBtn['command'] = lambda { onApply }
				okBtn = Tk::Tile::Button.new(okFrame){ text 'OK' }.pack :side => :right
					okBtn['command'] = lambda { onOK }
		end
end

if $0 == __FILE__
	app = UI.new
	app.run
end
